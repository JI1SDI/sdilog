# JI1SDI logger
This software is the qso logger for hamradio.

# Overview
This can

- Log

- (Print QSO cards)

# Usage

- (load-sdi-file "path")

- (save-sdi-file "path")

- (add-qso qso)
: QSOデータをデータベースに追加する.

- (make-qso :date :band :mode :sign :rrst :srst :qth :jcc-code :qra :memo)
: QSOデータを作る.追加はしない.

- (new-qso :date :band :mode :sign :rrst :srst :qth :jcc-code :qra :memo)
: QSOデータを作りデータベースに追加する.

# License
Copyright (c) 2017 JI1SDI