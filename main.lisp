;; If you use the quicklisp, you eval below command.
;;(load "~/quicklisp/setup.lisp")
(load "./file.lisp")

(defvar *db* nil)

(defun hello ()
  (format t "Hello, world."))

;; .sdi file is the qsl file format of this logger
;; this format is mainly S-expression.

(defun add-qso (qso)
  (setq *db* (append *db* (list qso))))

(defun make-qso (&key date band mode sign rrst srst qth jcc-code qra memo)
  (list date band mode sign rrst srst qth jcc-code qra memo))

(defun new-qso (&key date band mode sign rrst srst qth jcc-code qra memo)
  (add-qso (list date band mode sign rrst srst qth jcc-code qra memo)))


