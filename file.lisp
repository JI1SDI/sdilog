(defun save-sdi-file (path)
  (with-open-file (stream-out path :direction :output :if-exists :supersede)
    (print *db* stream-out)))

(defun load-sdi-file (path)
  ;; this function loads the logfile of .sdi
  (with-open-file (stream-in path :direction :input :if-does-not-exist nil)
    (setq *db* (read stream-in))))
